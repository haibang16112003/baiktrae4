﻿using System.ComponentModel.DataAnnotations;

namespace baiktra1.Models
{
    public class Customer
    {
        [Key]
        public int CustormerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public ICollection<Transactions> transactions { get; set; }
        public ICollection<Accounts> accounts { get; set; }

    }
}
