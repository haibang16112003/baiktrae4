﻿using System.ComponentModel.DataAnnotations;

namespace baiktra1.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int CustormerId { get; set; }
        public Customer Custormer { get; set; }
        public ICollection<Reports> reports { get; set; }


    }
}
