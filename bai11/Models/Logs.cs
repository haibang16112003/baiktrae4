﻿using System.ComponentModel.DataAnnotations;

namespace baiktra1.Models
{
    public class Logs
    {
        [Key]
        public int LogId { get; set; }
        public DateOnly LogDate { get; set; }
        public TimeOnly LogTime { get; set; }
        public int TransactionalId { get; set; }
        public Transactions Transactions { get; set; }
        public ICollection<Reports> reports { get; set; }

    }
}
