﻿using System.ComponentModel.DataAnnotations;

namespace baiktra1.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionalId { get; set; }
        public string Name { get; set; }

        public int CustormerId { get; set; }
        public Customer Custormer { get; set; }
        public int EmployeeId { get; set; }
        public Employees Employees { get; set; }
        public ICollection<Logs> logs { get; set; }
        public ICollection<Reports> reports { get; set; }


    }
}
