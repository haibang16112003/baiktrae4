﻿using System.ComponentModel.DataAnnotations;

namespace baiktra1.Models
{
    public class Reports
    {
        [Key]
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public DateOnly ReportDate { get; set; }
        public int AccountId { get; set; }
        public Accounts Accounts { get; set; }
        public int LogId { get; set; }
        public Logs Logs { get; set; }
        public int TransactionalId { get; set; }
        public Transactions Transactions { get; set; }

    }
}
