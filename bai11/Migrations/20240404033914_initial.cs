﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace bai11.Migrations
{
    /// <inheritdoc />
    public partial class initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Logindate",
                table: "Logs");

            migrationBuilder.DropColumn(
                name: "logintime",
                table: "Logs");

            migrationBuilder.RenameColumn(
                name: "EmloyeeId",
                table: "transactions",
                newName: "EmployeeId");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "transactions",
                newName: "CustormerId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "transactions",
                newName: "TransactionalId");

            migrationBuilder.RenameColumn(
                name: "Reportdate",
                table: "Reports",
                newName: "ReportDate");

            migrationBuilder.RenameColumn(
                name: "LogsId",
                table: "Reports",
                newName: "LogId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Reports",
                newName: "ReportId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Logs",
                newName: "LogId");

            migrationBuilder.RenameColumn(
                name: "Usernameandpassword",
                table: "employees",
                newName: "UserName");

            migrationBuilder.RenameColumn(
                name: "FisrtName",
                table: "employees",
                newName: "Password");

            migrationBuilder.RenameColumn(
                name: "Contracaddress",
                table: "employees",
                newName: "FirstName");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "employees",
                newName: "EmployeeId");

            migrationBuilder.RenameColumn(
                name: "Username",
                table: "customers",
                newName: "UserName");

            migrationBuilder.RenameColumn(
                name: "FisrtName",
                table: "customers",
                newName: "FirstName");

            migrationBuilder.RenameColumn(
                name: "Contracaddress",
                table: "customers",
                newName: "Contact");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "customers",
                newName: "CustormerId");

            migrationBuilder.RenameColumn(
                name: "Accountname",
                table: "Accounts",
                newName: "AccountName");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "Accounts",
                newName: "CustormerId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Accounts",
                newName: "AccountId");

            migrationBuilder.AlterColumn<DateOnly>(
                name: "ReportDate",
                table: "Reports",
                type: "date",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<DateOnly>(
                name: "LogDate",
                table: "Logs",
                type: "date",
                nullable: false,
                defaultValue: new DateOnly(1, 1, 1));

            migrationBuilder.AddColumn<TimeOnly>(
                name: "LogTime",
                table: "Logs",
                type: "time",
                nullable: false,
                defaultValue: new TimeOnly(0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "employees",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Contact",
                table: "employees",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "customers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_transactions_CustormerId",
                table: "transactions",
                column: "CustormerId");

            migrationBuilder.CreateIndex(
                name: "IX_transactions_EmployeeId",
                table: "transactions",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AccountId",
                table: "Reports",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_LogId",
                table: "Reports",
                column: "LogId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TransactionalId",
                table: "Reports",
                column: "TransactionalId");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_TransactionalId",
                table: "Logs",
                column: "TransactionalId");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CustormerId",
                table: "Accounts",
                column: "CustormerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_customers_CustormerId",
                table: "Accounts",
                column: "CustormerId",
                principalTable: "customers",
                principalColumn: "CustormerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Logs_transactions_TransactionalId",
                table: "Logs",
                column: "TransactionalId",
                principalTable: "transactions",
                principalColumn: "TransactionalId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Accounts_AccountId",
                table: "Reports",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "AccountId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Logs_LogId",
                table: "Reports",
                column: "LogId",
                principalTable: "Logs",
                principalColumn: "LogId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_transactions_TransactionalId",
                table: "Reports",
                column: "TransactionalId",
                principalTable: "transactions",
                principalColumn: "TransactionalId",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_transactions_customers_CustormerId",
                table: "transactions",
                column: "CustormerId",
                principalTable: "customers",
                principalColumn: "CustormerId",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_transactions_employees_EmployeeId",
                table: "transactions",
                column: "EmployeeId",
                principalTable: "employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_customers_CustormerId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Logs_transactions_TransactionalId",
                table: "Logs");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Accounts_AccountId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Logs_LogId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_transactions_TransactionalId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_transactions_customers_CustormerId",
                table: "transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_transactions_employees_EmployeeId",
                table: "transactions");

            migrationBuilder.DropIndex(
                name: "IX_transactions_CustormerId",
                table: "transactions");

            migrationBuilder.DropIndex(
                name: "IX_transactions_EmployeeId",
                table: "transactions");

            migrationBuilder.DropIndex(
                name: "IX_Reports_AccountId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_LogId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TransactionalId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Logs_TransactionalId",
                table: "Logs");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_CustormerId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "LogDate",
                table: "Logs");

            migrationBuilder.DropColumn(
                name: "LogTime",
                table: "Logs");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "employees");

            migrationBuilder.DropColumn(
                name: "Contact",
                table: "employees");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "customers");

            migrationBuilder.RenameColumn(
                name: "EmployeeId",
                table: "transactions",
                newName: "EmloyeeId");

            migrationBuilder.RenameColumn(
                name: "CustormerId",
                table: "transactions",
                newName: "CustomerId");

            migrationBuilder.RenameColumn(
                name: "TransactionalId",
                table: "transactions",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ReportDate",
                table: "Reports",
                newName: "Reportdate");

            migrationBuilder.RenameColumn(
                name: "LogId",
                table: "Reports",
                newName: "LogsId");

            migrationBuilder.RenameColumn(
                name: "ReportId",
                table: "Reports",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "LogId",
                table: "Logs",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "employees",
                newName: "Usernameandpassword");

            migrationBuilder.RenameColumn(
                name: "Password",
                table: "employees",
                newName: "FisrtName");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "employees",
                newName: "Contracaddress");

            migrationBuilder.RenameColumn(
                name: "EmployeeId",
                table: "employees",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "customers",
                newName: "Username");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "customers",
                newName: "FisrtName");

            migrationBuilder.RenameColumn(
                name: "Contact",
                table: "customers",
                newName: "Contracaddress");

            migrationBuilder.RenameColumn(
                name: "CustormerId",
                table: "customers",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "AccountName",
                table: "Accounts",
                newName: "Accountname");

            migrationBuilder.RenameColumn(
                name: "CustormerId",
                table: "Accounts",
                newName: "CustomerId");

            migrationBuilder.RenameColumn(
                name: "AccountId",
                table: "Accounts",
                newName: "Id");

            migrationBuilder.AlterColumn<string>(
                name: "Reportdate",
                table: "Reports",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(DateOnly),
                oldType: "date");

            migrationBuilder.AddColumn<string>(
                name: "Logindate",
                table: "Logs",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "logintime",
                table: "Logs",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
